import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class MainApplication extends Application {
    public void start(Stage primaryStage) throws Exception {

        primaryStage.setTitle("GUI Application");

        Parent root = FXMLLoader.load(getClass().getResource("main.fxml"));

        Scene scene = new Scene(root, 1024, 860);

        primaryStage.setScene(scene);

        primaryStage.show();
    }
    public static void main(String[] args ){launch(args);}
}

