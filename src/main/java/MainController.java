import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class MainController {

    @FXML
    Label label;
    @FXML
    Button button;
    @FXML
    TextField edit;

    @FXML
    private void handleClik() {
        label.setText(edit.getText());
    }
}
